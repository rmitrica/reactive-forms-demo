import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss']
})
export class FormGroupComponent implements OnInit {

  inputFromGroup: FormGroup;

  inputDisabled: boolean;

  constructor() { }

  ngOnInit() {
    this.inputFromGroup =  new FormGroup({
      simpleInput: new FormControl({value: 'Initial Value from form', disabled: false})
    });
  }

  onChange($event) {
    console.log('FormGroupComponent:onChange', $event);
  }

  disabledChange($event) {
    console.log($event);

    this.inputDisabled = !this.inputDisabled;

    if (this.inputDisabled) {
      this.inputFromGroup.get('simpleInput').disable();
    } else  {
      this.inputFromGroup.get('simpleInput').enable();
    }
  }

  setControlValue() {
    this.inputFromGroup.controls['simpleInput'].setValue('This is set programaticaly');
  }


}
