import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleComponent } from './simple/simple.component';
import {ComponentsModule} from '../components/components.module';
import { FormGroupComponent } from './form-group/form-group.component';
import {ReactiveFormsModule} from '@angular/forms';
import { FormValidationComponent } from './form-validation/form-validation.component';



@NgModule({
  declarations: [SimpleComponent, FormGroupComponent, FormValidationComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ReactiveFormsModule
  ]
})
export class ViewsModule { }
