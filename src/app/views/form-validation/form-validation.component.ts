import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';

export function forbiddenValueValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = nameRe.test(control.value);
    return forbidden ? {'forbiddenValue': {value: control.value}} : null;
  };
}

@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.scss']
})
export class FormValidationComponent implements OnInit {

  inputFromGroup: FormGroup;

  inputDisabled: boolean;

  constructor() { }

  ngOnInit() {
    this.inputFromGroup =  new FormGroup({
      simpleInput: new FormControl(
        'Initial Value',
      [
        Validators.required,
        Validators.minLength(3)
      ]),
      rfaInput: new FormControl('',
        [
          Validators.required,
          Validators.minLength(3),
          forbiddenValueValidator(/aa/i)
        ])
    });
  }

  onChange($event) {
    console.log('FormGroupComponent:onChange', $event);
  }

  get simpleInput() {
    return this.inputFromGroup.get('simpleInput');
  }

  get rfaInput() {
    return this.inputFromGroup.get('rfaInput');
  }


}
