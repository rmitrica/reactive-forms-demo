import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SimpleComponent} from './views/simple/simple.component';
import {FormGroupComponent} from './views/form-group/form-group.component';
import {FormValidationComponent} from './views/form-validation/form-validation.component';


const routes: Routes = [
  {
    path: 'simple-input',
    component: SimpleComponent
  },
  {
    path: 'form-group',
    component: FormGroupComponent
  },
  {
    path: 'form-validation',
    component: FormValidationComponent
  },
  { path: '',
    redirectTo: '/simple-input',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
