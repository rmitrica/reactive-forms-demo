import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-rfa-input',
  templateUrl: './rfa-input.component.html',
  styleUrls: ['./rfa-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RfaInputComponent),
      multi: true
    }
  ]
})
export class RfaInputComponent implements OnInit, ControlValueAccessor {

  @Input() placeholder = 'Placeholder';

  public disabled: boolean;
  private _value: any;

  constructor() { }

  ngOnInit() {
  }

  onChanged: any = (value: any) => {};
  onTouched: any = () => {};

  registerOnChange(fn: any): void {
    this.onChanged = fn;
    console.log('RfaInputComponent:registerOnChange', fn);
  }

  registerOnTouched(fn: any): void {
    this.onTouched =  fn;
    console.log('RfaInputComponent:registerOnTouched', fn);
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled =  isDisabled;
    console.log('RfaInputComponent:setDisabledState', isDisabled);
  }

  writeValue(val: any): void {
    this._value = val;
    console.log('RfaInputComponent:writeValue', val);
  }

  set value(val: any) {
    this._value = val;
    this.onChanged(val);
  }

  get value() {
    return this._value;
  }

}
