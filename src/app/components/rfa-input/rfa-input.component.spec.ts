import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RfaInputComponent } from './rfa-input.component';

describe('RfaInputComponent', () => {
  let component: RfaInputComponent;
  let fixture: ComponentFixture<RfaInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfaInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RfaInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
