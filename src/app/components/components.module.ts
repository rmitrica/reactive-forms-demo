import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RfaInputComponent } from './rfa-input/rfa-input.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [RfaInputComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    RfaInputComponent
  ]
})
export class ComponentsModule { }
