import {RfaInputComponent} from '../app/components/rfa-input/rfa-input.component';

export default {
  title: 'rfa-input',
};

export const defaultRfaInput = () => ({
  component: RfaInputComponent,
});

defaultRfaInput.story = {
  parameters: { notes: 'My notes on a button with emojis' },
}
